﻿using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;

namespace DockerWorkshopWeb
{
    public class AppDb
    {
        private readonly IConfiguration _configuration;

        public AppDb(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public MySqlConnection GetMySqlConnection()
        {
            var connection = new MySqlConnection(_configuration["connection_string"]);
            connection.Open();
            return connection;
        }
    }
}
