﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using DockerWorkshopWeb.Models;
using Microsoft.Extensions.Configuration;

namespace DockerWorkshopWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly AppDb _appDb;

        public HomeController(IConfiguration configuration, AppDb appDb)
        {
            _configuration = configuration;
            _appDb = appDb;
        }

        public IActionResult HelloWorld()
        {
            return Content("Hello, World!");
        }

        public async Task<IActionResult> Index(int id)
        {
            try
            {
                using (var conn = _appDb.GetMySqlConnection())
                {
                    var cmd = conn.CreateCommand();
                    cmd.CommandText = @"SELECT `Id`, `Value` FROM `Table1`";
                    var model = await ReadAllAsync(await cmd.ExecuteReaderAsync());
                    return View(model);
                }
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }
        }

        public async Task<IActionResult> Get(int id)
        {
            try
            {
                using (var conn = _appDb.GetMySqlConnection())
                {
                    var cmd = conn.CreateCommand();
                    cmd.CommandText = @"SELECT `Id`, `Value` FROM `Table1` WHERE `Id` = @id";
                    cmd.Parameters.AddWithValue("@id", id);
                    var model = await ReadAllAsync(await cmd.ExecuteReaderAsync());
                    return View("Index", model);
                }
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }
        }

        public async Task<ActionResult> Insert(string value)
        {
            try
            {
                using (var conn = _appDb.GetMySqlConnection())
                {
                    var cmd = conn.CreateCommand();
                    cmd.CommandText = @"INSERT INTO `Table1` (`Value`) VALUES (@value);";
                    cmd.Parameters.AddWithValue("@value", value);
                    await cmd.ExecuteNonQueryAsync();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }
        }

        public async Task<ActionResult> Update(int id, string value)
        {
            try
            {
                using (var conn = _appDb.GetMySqlConnection())
                {
                    var cmd = conn.CreateCommand();
                    cmd.CommandText = @"UPDATE `Table1` SET `Value` = @value WHERE `Id` = @id;";
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@value", value);
                    await cmd.ExecuteNonQueryAsync();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }

        }

        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                using (var conn = _appDb.GetMySqlConnection())
                {
                    var cmd = conn.CreateCommand();
                    cmd.CommandText = @"DELETE FROM `Table1` WHERE `Id` = @id;";
                    cmd.Parameters.AddWithValue("@id", id);
                    await cmd.ExecuteNonQueryAsync();
                    return RedirectToAction("Index");
                }
            }
            catch (Exception ex)
            {
                return Content(ex.ToString());
            }
        }

        private async Task<List<TableModel>> ReadAllAsync(System.Data.Common.DbDataReader reader)
        {
            var posts = new List<TableModel>();
            using (reader)
            {
                while (await reader.ReadAsync())
                {
                    var post = new TableModel()
                    {
                        Id = await reader.GetFieldValueAsync<int>(0),
                        Value = await reader.GetFieldValueAsync<string>(1),
                    };
                    posts.Add(post);
                }
            }
            return posts;
        }
    }
}
