﻿namespace DockerWorkshopWeb.Models
{
    public class TableModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
